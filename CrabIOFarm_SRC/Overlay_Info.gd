extends Control
# Определяю шрифты
var Font_Sys = DynamicFont.new()
var Font_Perf = DynamicFont.new()
var Font_License = DynamicFont.new()

func _ready():
	#Шрифт системной информации
	Font_Sys.font_data = load("res://base_font/GNU Free Font/FreeSansBold.ttf")
	Font_Sys.size = 20
	Font_Sys.outline_size = 2
	Font_Sys.outline_color = Color(0.25, 0.25, 0.4, 1)
	$SysInfo.set("custom_fonts/font", Font_Sys)
	#Шрифт информации о производительности
	Font_Perf.font_data = load("res://base_font/GNU Free Font/FreeSansBold.ttf")
	Font_Perf.size = 22
	Font_Perf.outline_size = 2
	Font_Perf.outline_color = Color(0.08, 0.08, 0.2, 1)
	$PerfInfo.set("custom_fonts/font", Font_Perf)
	$PerfInfo.set("custom_colors/font_color", Color(1, 1, 0.75, 1))
	#Шрифт лицензии
	Font_License.font_data = load("res://base_font/FreeMono/FreeMonospacedBold.ttf")
	Font_License.size = 17
	Font_License.outline_size = 1
	Font_License.outline_color = Color(0.08, 0.08, 0.08, 1)
	$License.set("custom_fonts/font", Font_License)
	$HotKeys.set("custom_fonts/font", Font_License)


func _process(delta):
		# Обворачиваю функцией "str()" чтобы значение функции "Pergormance.get_monitor()" всегда
	# приводилось в текстовый вид, таким образом заранее исключаю вероятность подачи неподходящего
	# значения в переменную содержающую строку, тем самым избегаю возможных ошибок.
	#
	# Получение данных FPS.
	var Perf_FPS = "\nFPS: " + str(Performance.get_monitor(Performance.TIME_FPS))
	# Получение данных про 2D часть рендера.
	var Info_2D_DrawCalls = "\nDrawCalls: " + str(Performance.get_monitor(Performance.RENDER_2D_DRAW_CALLS_IN_FRAME))
	var Info_2D_ItemsFrame = "\nItems in frame: " + str(Performance.get_monitor(Performance.RENDER_2D_ITEMS_IN_FRAME))
	# Получение данных про 3D часть рендера.
	var Info_3D_DrawCalls = "\nDrawCalls " + str(Performance.get_monitor(Performance.RENDER_DRAW_CALLS_IN_FRAME))
	var Info_3D_ObjectsFrame = "\nObjects in frame: " + str(Performance.get_monitor(Performance.RENDER_OBJECTS_IN_FRAME))
	var Info_3D_VertsFrame = "\nVertices in frame: " + str(Performance.get_monitor(Performance.RENDER_VERTICES_IN_FRAME))
	
	# Экспериментальные строки.
	var Info_Audio_OutLat = "\nAudio output latency: " + str(Performance.get_monitor(Performance.AUDIO_OUTPUT_LATENCY))
	
	#Разрешение рендера
	var Info_renderX = OS.get_window_safe_area().size.x
	var Info_renderY = OS.get_window_safe_area().size.y
	
	# Получение основной технической информации.
	var Info_Sys_DRV = "\nAPI: " + str(OS.get_video_driver_name(OS.get_current_video_driver()))
	var Info_Sys_ScreenSize = "\nScreen size: " + str(OS.get_screen_size())
	var Info_Sys_FullWindowSize = "\nFull Window Size: " + str(OS.get_real_window_size())
	var Info_Sys_RenderSize = "\nRender Size: " + str(Info_renderX) + " x " + str(Info_renderY)
	
	#Вывод текста в 2D обьекты "Label"
	#Выбираю название 2D обьекта с помощью "$" и через точку выбираю свойство которому присваиваю значение с помощью "=".
	$PerfInfo.text = "__Производительность__" + Perf_FPS + "\n___2D___" + Info_2D_DrawCalls + Info_2D_ItemsFrame + "\n___3D___" + Info_3D_DrawCalls + Info_3D_ObjectsFrame + Info_3D_VertsFrame
	$SysInfo.text = "__Системная информация__" + Info_Sys_DRV + Info_Sys_ScreenSize + Info_Sys_FullWindowSize + Info_Sys_RenderSize + Info_Audio_OutLat
	
	#Определение размера и положение обьекта "SysInfo".
	$SysInfo.margin_left = Info_renderX / 2
	$SysInfo.margin_top = 0
	$SysInfo.margin_right = Info_renderX - 5
	$SysInfo.margin_bottom = Info_renderY / 2
	#Определение размера и положение обьекта "PerfInfo".
	$PerfInfo.margin_left = 5
	$PerfInfo.margin_top = 0
	$PerfInfo.margin_right = Info_renderX / 2
	$PerfInfo.margin_bottom = Info_renderY / 2
	#Определение размера и положение обьекта "License".
	$License.margin_left = 0
	$License.margin_top = Info_renderY - 90
	$License.margin_right = Info_renderX
	$License.margin_bottom = Info_renderY
	#Определение размера и положение обьекта "HotKeys".
	$HotKeys.margin_left = 0
	$HotKeys.margin_top = Info_renderY / 2
	$HotKeys.margin_right = Info_renderX / 2
	$HotKeys.margin_bottom = Info_renderY / 2 + 100

	#Грубое но рабочее решение для масштабирования текста в зависимости от разрешения.
	if Info_renderX >= 2200:
		Font_Sys.size = 32
		Font_Perf.size = 34
		Font_License.size = 22
	if Info_renderX >= 1600 and Info_renderX < 2200:
		Font_Sys.size = 26
		Font_Perf.size = 28
		Font_License.size = 20
	if Info_renderX > 1000 and Info_renderX < 1600:
		Font_Sys.size = 20
		Font_Perf.size = 22
		Font_License.size = 18
	if Info_renderX <= 1000:
		Font_Sys.size = 16
		Font_Perf.size = 18
		Font_License.size = 16
