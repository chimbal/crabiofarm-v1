extends KinematicBody

const Gravity = -10
const Speed = 8
var move_speed = 0
var fwd_speed = 0
var str_speed = 0
var Velocity = Vector3()

func _physics_process(delta):
	var Direction = Vector3()
	

	if move_speed:
		Direction.z = move_speed * Speed
		Direction = Direction.rotated(Vector3(0,1,0), rotation.y)
		
	Velocity.x = Direction.x
	Velocity.z = Direction.z
	
	Velocity.y += Gravity + delta
	
	Velocity = move_and_slide(Velocity, Vector3(0,1,0))
