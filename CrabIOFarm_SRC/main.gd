extends Node

#Эта информация не требует постоянного обновления, потому определена здесь в виде константы.
const GNU_GPLv3 = "CrabIOFarm v0.2.0 (GPLv3)"
const control_keys = "Hot Keys:\nHide Info Overlay: \"Home\""
func _ready():
	#Вывод версии игры и лицензии
	$Overlay_Info/License.text = GNU_GPLv3
	$Overlay_Info/HotKeys.text = control_keys

# Делаю внутри _process(delta): чтобы оно постоянно обновлялось во время работы игры.
func _process(delta):

	#Переключение видимости оверлея с информацией
	if Input.is_action_just_pressed("ui_home"):
		if $Overlay_Info.visible == true:
			$Overlay_Info.visible = false;
		else:
			$Overlay_Info.visible = true;
