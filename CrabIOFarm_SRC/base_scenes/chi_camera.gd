extends Position3D

export(NodePath) var target_node # цель управления
export(float) var distance = 3 # расстояние до обьекта
export(float) var head_height = 0 # высота головы
var target
var camera
var chi_rotationY = 0
var chi_velX
var chi_velZ

func _ready():
	target = get_node(target_node)
	camera = $Camera
	$Camera.transform.origin.z = distance
	$cam_collision.cast_to.z = distance

func _process(delta):
	# привязка камеры к обьекту и поднятие на нужный уровень головы
	transform.origin = target.transform.origin + Vector3(0, head_height, 0)
	target.move_speed = 0
	# управление
	if Input.is_action_pressed("ui_up"):
		target.move_speed = -1
		chi_rotationY = rotation.y
	if Input.is_action_pressed("ui_down"):
		target.move_speed = -1
		chi_rotationY = rotation.y - PI
	if Input.is_action_pressed("ui_left"):
		target.move_speed = -1
		chi_rotationY = rotation.y + PI/2
	if Input.is_action_pressed("ui_right"):
		target.move_speed = -1
		chi_rotationY = rotation.y - PI/2
	
	# сглаживание поворота движения
	if target.move_speed:
		target.transform.basis = Basis(target.transform.basis.get_rotation_quat().slerp(Basis(Vector3.UP, chi_rotationY).get_rotation_quat(), 8 * delta))
	
	# столкновение камеры с обьектами имеющими коллизию
	if $cam_collision.is_colliding():
		camera.transform.origin.z = $cam_collision.get_collision_point().distance_to(global_transform.origin) - 0.5
	else:
	# возвращение камеры в исходное положение
		camera.transform.origin.z = distance

func _input(event):
	if event is InputEventMouseMotion:
		rotation.y -= event.relative.x * 0.01
		rotation.x = clamp(rotation.x - event.relative.y * 0.01, -1, 1)
		
